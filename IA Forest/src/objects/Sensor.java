package objects;

import objects.Elements.Element;

public class Sensor {
	public boolean isWind(Elements cell) {
		return cell.getList().contains(Element.WIND);
	}

	public boolean isSmell(Elements cell) {
		return cell.getList().contains(Element.SMELL);
	}

	public boolean isLight(Elements cell) {
		return cell.getList().contains(Element.LIGHT);
	}
}