package objects;

public class Forest {
	private Elements[][] grid;
	private int size;

	public Forest() {

	}

	public Elements[][] getGrid() {
		return grid;
	}

	public void setGrid(Elements[][] grid) {
		this.grid = grid;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}