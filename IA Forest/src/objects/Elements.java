package objects;

import java.util.HashSet;

public class Elements {
	public enum Element {
		SMELL, WIND, MONSTER, HOLE, LIGHT, PORTAL, PLAYER
	}

	private int x;
	private int y;

	private HashSet<Element> list;

	public Elements(int x, int y) {
		this.list = new HashSet<Element>();
		this.x = x;
		this.y = y;
	}

	public HashSet<Element> getList() {
		return list;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public void setList(HashSet<Element> list) {
		this.list = list;
	}

	public void addToList(Element e) {
		this.list.add(e);
	}

	public void removeFromList(Element player) {
		this.list.remove(player);
	}
}