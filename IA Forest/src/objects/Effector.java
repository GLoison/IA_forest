package objects;

import objects.Elements.Element;

public class Effector {
	public Elements move(Environment env, int x, int y, Elements player) {
		env.getForest().getGrid()[x][y].addToList(Element.PLAYER);
		player.removeFromList(Element.PLAYER);
		return env.getForest().getGrid()[x][y];
	}

	public void shoot(Environment env, int x, int y) {
		env.getForest().getGrid()[x][y].removeFromList(Element.MONSTER);
	}

	public void leave(Environment env) {
		env.newEnv();
	}
}