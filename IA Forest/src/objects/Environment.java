package objects;

import java.util.HashSet;
import java.util.Random;
import objects.Elements.Element;

public class Environment {
	private Forest forest;

	private Elements spawn;

	public Environment() {
		this.forest = new Forest();
		this.forest.setSize(3);
		fillForest();
	}

	public void newEnv() {
		int newSize = this.forest.getSize() + 1;
		this.forest = new Forest();
		this.forest.setSize(newSize);
		fillForest();
	}

	private void fillForest() {
		this.forest.setGrid(createGrid());
	}

	private Elements[][] createGrid() {
		Elements[][] grid = new Elements[this.forest.getSize()][this.forest.getSize()];
		addMonsterAndCrevasse(grid);
		addSmellAndWind(grid);
		addPortal(grid);
		addPlayer(grid);
		return grid;
	}

	private void addPlayer(Elements[][] grid) {
		boolean havePlayer = false;
		while (!havePlayer) {
			int rngX = new Random().nextInt(this.forest.getSize());
			int rngY = new Random().nextInt(this.forest.getSize());
			if (!grid[rngX][rngY].getList().contains(Element.HOLE)
					&& !grid[rngX][rngY].getList().contains(Element.MONSTER)
					&& !grid[rngX][rngY].getList().contains(Element.PORTAL)) {
				grid[rngX][rngY].addToList(Element.PLAYER);
				this.spawn = grid[rngX][rngY];
				havePlayer = true;
			}
		}
	}

	private void addPortal(Elements[][] grid) {
		boolean havePortal = false;
		while (!havePortal) {
			int rngX = new Random().nextInt(this.forest.getSize());
			int rngY = new Random().nextInt(this.forest.getSize());
			if (!grid[rngX][rngY].getList().contains(Element.HOLE)
					&& !grid[rngX][rngY].getList().contains(Element.MONSTER)) {
				grid[rngX][rngY].getList().clear();
				grid[rngX][rngY].addToList(Element.PORTAL);
				grid[rngX][rngY].addToList(Element.LIGHT);
				havePortal = true;
			}
		}
	}

	private void addSmellAndWind(Elements[][] grid) {
		for (int i = 0; i < this.forest.getSize(); i++)
			for (int j = 0; j < this.forest.getSize(); j++) {
				HashSet<Element> temp = grid[i][j].getList();
				if (temp.contains(Element.MONSTER))
					addSmell(grid, i, j);
				if (temp.contains(Element.HOLE))
					addWind(grid, i, j);
			}
	}

	private void addMonsterAndCrevasse(Elements[][] grid) {
		for (int i = 0; i < this.forest.getSize(); i++)
			for (int j = 0; j < this.forest.getSize(); j++) {
				Elements e = new Elements(i, j);
				isMonster(e);
				isCrevasse(e);
				grid[i][j] = e;
			}
	}

	private void addWind(Elements[][] grid, int i, int j) {
		if (i - 1 >= 0)
			grid[i - 1][j].addToList(Element.WIND);
		if (j - 1 >= 0)
			grid[i][j - 1].addToList(Element.WIND);
		if (i + 1 < this.forest.getSize())
			grid[i + 1][j].addToList(Element.WIND);
		if (j + 1 < this.forest.getSize())
			grid[i][j + 1].addToList(Element.WIND);
	}

	private void addSmell(Elements[][] grid, int i, int j) {
		if (i - 1 >= 0)
			grid[i - 1][j].addToList(Element.SMELL);
		if (j - 1 >= 0)
			grid[i][j - 1].addToList(Element.SMELL);
		if (i + 1 < this.forest.getSize())
			grid[i + 1][j].addToList(Element.SMELL);
		if (j + 1 < this.forest.getSize())
			grid[i][j + 1].addToList(Element.SMELL);
	}

	private void isCrevasse(Elements e) {
		double rng = Math.random() * 100;
		if (rng <= 10)
			e.addToList(Element.HOLE);
	}

	private void isMonster(Elements e) {
		double rng = Math.random() * 100;
		if (rng <= 10)
			e.addToList(Element.MONSTER);
	}

	public Forest getForest() {
		return forest;
	}

	public void setForest(Forest forest) {
		this.forest = forest;
	}

	public Elements getPlayer() {
		return this.spawn;
	}

	public void updateEnv() {
		for (int i = 0; i < this.forest.getSize(); i++)
			for (int j = 0; j < this.forest.getSize(); j++) {
				HashSet<Element> temp = this.forest.getGrid()[i][j].getList();
				if (temp.contains(Element.SMELL)) {
					checkForMonster(i, j);
				}
			}
	}

	private void checkForMonster(int i, int j) {
		boolean b = false;
		if (i != 0)
			if (this.forest.getGrid()[i - 1][j].getList().contains(Element.MONSTER))
				b = true;
		if (i < this.forest.getSize() - 1)
			if (this.forest.getGrid()[i + 1][j].getList().contains(Element.MONSTER))
				b = true;
		if (j != 0)
			if (this.forest.getGrid()[i][j - 1].getList().contains(Element.MONSTER))
				b = true;
		if (j < this.forest.getSize() - 1)
			if (this.forest.getGrid()[i][j + 1].getList().contains(Element.MONSTER))
				b = true;

		if (!b)
			this.forest.getGrid()[i][j].getList().remove(Element.SMELL);

	}
}