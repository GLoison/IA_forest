package objects;

import java.util.ArrayList;
import java.util.List;
import facts.Facts;
import facts.Facts.Fact;
import objects.Elements.Element;
import rules.Actions;

public class Agent {
	private Environment environment;
	private Elements player;
	private Sensor sensor;
	private Effector effector;
	private List<Elements> knownEnv;
	private List<Facts> factsList;
	private int performance;

	public Agent(Environment _environment) {
		this.environment = _environment;
		this.player = this.environment.getPlayer();
		this.knownEnv = new ArrayList<Elements>();
		this.addKnownElement(player);
		this.factsList = new ArrayList<Facts>();
		this.sensor = new Sensor();
		this.effector = new Effector();
		for (Elements[] element : this.environment.getForest().getGrid()) {
			for (Elements e : element) {
				if (!knownEnv.contains(e)) {
					this.factsList.add(new Facts(e.getX(), e.getY(), Fact.NOTVISITED));
				}
			}
		}
		this.updateFacts();
	}

	public void reset() {
		this.player = this.environment.getPlayer();
		this.knownEnv = new ArrayList<Elements>();
		this.addKnownElement(player);
		this.factsList = new ArrayList<Facts>();
		for (Elements[] element : this.environment.getForest().getGrid()) {
			for (Elements e : element) {
				if (!knownEnv.contains(e)) {
					this.factsList.add(new Facts(e.getX(), e.getY(), Fact.NOTVISITED));
				}
			}
		}
		this.updateFacts();
	}

	public List<Elements> getKnownElements() {
		return this.knownEnv;
	}

	private void addKnownElement(Elements e) {
		this.knownEnv.add(e);
	}

	public void processAction(List<Actions> actions) {
		for (Actions action : actions) {
			switch (action.getAction()) {
			case LEAVE:
				this.performance += 10 * this.environment.getForest().getSize()*this.environment.getForest().getSize();
				this.effector.leave(this.environment);
				this.reset();
				break;
			case MOVETO:
				this.player = this.effector.move(this.environment, action.getX(), action.getY(), player);
				this.performance--;
				this.addKnownElement(player);
				this.updateFacts();
				if (player.getList().contains(Element.HOLE)) {
					this.player = this.effector.move(environment, this.environment.getPlayer().getX(),
							this.environment.getPlayer().getY(), player);
					this.performance -= 10 * this.environment.getForest().getSize()*this.environment.getForest().getSize();
				}
				break;
			case SHOOT:
				this.effector.shoot(this.environment, action.getX(), action.getY());
				this.performance -= 10;
				this.environment.updateEnv();
				break;
			default:
				break;
			}
		}
	}

	public void updateFacts() {
		removeFromFacts();
		if (this.sensor.isLight(player)) {
			this.factsList.add(new Facts(player.getX(), player.getY(), Fact.LIGHT));
		} else if (this.sensor.isSmell(player)) {
			this.factsList.add(new Facts(player.getX(), player.getY(), Fact.SMELL));
		} else if (this.sensor.isWind(player)) {
			this.factsList.add(new Facts(player.getX(), player.getY(), Fact.WIND));
		} else {
			this.factsList.add(new Facts(player.getX(), player.getY(), Fact.EMPTY));
		}
	}

	public void removeFromFacts() {
		List<Facts> toDelete = new ArrayList<Facts>();
		for (Facts f : this.factsList) {
			if (f.getFact() == Fact.NOTVISITED && f.getX() == player.getX() && f.getY() == player.getY()) {
				toDelete.add(f);
			}
		}
		for (Facts f : toDelete) {
			this.factsList.remove(f);
		}
	}

	public ArrayList<Facts> getFactsList() {
		return (ArrayList<Facts>) this.factsList;
	}

	public int getPerformance() {
		return performance;
	}

	public void setPerformance(int performance) {
		this.performance = performance;
	}
}