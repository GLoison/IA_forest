package rules;

public class Actions {
	public enum Action {
		MOVETO, SHOOT, LEAVE
	}

	private int x;
	private int y;
	private Action action;

	public Actions(int _x, int _y, Action _action) {
		this.x = _x;
		this.y = _y;
		this.action = _action;
	}

	public Actions(Action _action) {
		this.action = _action;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public Action getAction() {
		return this.action;
	}

	public String toString() {
		return this.action.toString();
	}
}