package rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import facts.Facts;
import objects.Elements;

public class RulesBase {
	private List<Rules> rules;

	public RulesBase(List<Elements> list) {
		this.rules = new ArrayList<Rules>();
		this.fillList(list);
	}

	private void fillList(List<Elements> list) {
		for (Elements e : list) {
			int x = e.getX();
			int y = e.getY();

			// ADD LEAVE Action
			rules.add(new Rules(new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.LIGHT))),
					new ArrayList<Actions>(Arrays.asList(new Actions(Actions.Action.LEAVE))), 10));

			// ADD MOVETO Action
			// left
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.EMPTY),
							new Facts(x - 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x - 1, y, Actions.Action.MOVETO))), 5));
			// top
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.EMPTY),
							new Facts(x, y - 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y - 1, Actions.Action.MOVETO))), 5));
			// right
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.EMPTY),
							new Facts(x + 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x + 1, y, Actions.Action.MOVETO))), 5));
			// bottom
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.EMPTY),
							new Facts(x, y + 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y + 1, Actions.Action.MOVETO))), 5));

			// left
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.SMELL),
							new Facts(x - 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x - 1, y, Actions.Action.SHOOT),
							new Actions(x - 1, y, Actions.Action.MOVETO))),
					3));
			// top
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.SMELL),
							new Facts(x, y - 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y - 1, Actions.Action.SHOOT),
							new Actions(x, y - 1, Actions.Action.MOVETO))),
					3));
			// right
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.SMELL),
							new Facts(x + 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x + 1, y, Actions.Action.SHOOT),
							new Actions(x + 1, y, Actions.Action.MOVETO))),
					3));
			// bottom
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.SMELL),
							new Facts(x, y + 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y + 1, Actions.Action.SHOOT),
							new Actions(x, y + 1, Actions.Action.MOVETO))),
					3));

			// left
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.WIND),
							new Facts(x - 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x - 1, y, Actions.Action.MOVETO))), 1));
			// top
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.WIND),
							new Facts(x, y - 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y - 1, Actions.Action.MOVETO))), 1));
			// right
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.WIND),
							new Facts(x + 1, y, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x + 1, y, Actions.Action.MOVETO))), 1));
			// bottom
			rules.add(new Rules(
					new ArrayList<Facts>(Arrays.asList(new Facts(x, y, Facts.Fact.WIND),
							new Facts(x, y + 1, Facts.Fact.NOTVISITED))),
					new ArrayList<Actions>(Arrays.asList(new Actions(x, y + 1, Actions.Action.MOVETO))), 1));
		}
	}

	public ArrayList<Rules> getRules() {
		return (ArrayList<Rules>) this.rules;
	}
//	// return true if the case at (x;y) has been visited
//	public boolean isVisited(List<Elements> list, int x, int y) {
//		for (Elements e : list) {
//			if (e.getX() == x && e.getY() == y) {
//				return true;
//			}
//		}
//		return false;
//	}
}