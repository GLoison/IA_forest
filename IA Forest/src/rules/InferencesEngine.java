package rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import facts.Facts;
import objects.Agent;

public class InferencesEngine {
	private Agent agent;

	public InferencesEngine(Agent _agent) {
		this.agent = _agent;
	}

	public void process() {
		RulesBase rules = new RulesBase(this.agent.getKnownElements());
		ArrayList<Facts> factList = this.agent.getFactsList();
		ArrayList<Rules> rulesList = rules.getRules();
		ArrayList<Rules> okRules = new ArrayList<Rules>();
		int maxWeight = 0;
		for (Rules rule : rulesList) {
			if (isSatisfyingAllConditions(factList, rule.getCondition())) {
				if (rule.getWeight() > maxWeight) {
					okRules.clear();
					okRules.add(rule);
					maxWeight = rule.getWeight();
				} else if (rule.getWeight() == maxWeight) {
					okRules.add(rule);
				}
			}
		}
		Random random = new Random();
		int rand = random.nextInt(okRules.size());
		List<Actions> actions = okRules.get(rand).getActions();
		this.agent.processAction(actions);
	}

	public boolean isSatisfyingAllConditions(ArrayList<Facts> factList, ArrayList<Facts> conditions) {
		for (Facts c : conditions) {
			boolean b = false;
			for (Facts f : factList) {
				if (c.getFact() == f.getFact() && c.getX() == f.getX() && c.getY() == f.getY()) {
					b = true;
				}
			}
			if (!b) {
				return false;
			}
		}
		return true;
	}
}