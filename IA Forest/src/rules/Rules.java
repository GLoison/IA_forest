package rules;

import java.util.ArrayList;

import facts.Facts;

public class Rules {
	private ArrayList<Facts> conditions;
	private ArrayList<Actions> actions;
	int weight;

	public Rules(ArrayList<Facts> conditions, ArrayList<Actions> actions, int _weight) {
		this.conditions = conditions;
		this.actions = actions;
		this.weight = _weight;
	}

	public ArrayList<Facts> getCondition() {
		return conditions;
	}

	public ArrayList<Actions> getActions() {
		return actions;
	}

	public int getWeight() {
		return this.weight;
	}

	public String toString() {
		return this.conditions + " => " + this.actions;
	}
}