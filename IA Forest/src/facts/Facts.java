package facts;

public class Facts {
	public enum Fact {
		EMPTY, VISITED, NOTVISITED, SMELL, LIGHT, WIND
	}

	private int x;
	private int y;
	private Fact fact;

	public Facts(int _x, int _y, Fact _fact) {
		this.x = _x;
		this.y = _y;
		this.fact = _fact;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public Fact getFact() {
		return this.fact;
	}

	public String toString() {
		return this.fact.toString() + "x = " + x + ", y = " + y;
	}
}