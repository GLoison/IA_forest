package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JButton;

public class Button extends JButton {
	private static final long serialVersionUID = 1L;

	protected int w;
	protected int h;
	protected int r;

	protected Color borderColor;
	protected Color normalColor;
	protected Color pressedColor;
	protected Color overColor;

	protected String text;
	protected Color textNormalColor;
	protected Color textPressedColor;
	protected Color textOverColor;
	protected int textSize;
	protected String fontName;

	protected boolean mousePressed = false;
	protected boolean mouseOver = false;

	private Main main;

	public Button(String text, Main main) {
		this.main = main;
		this.w = Main.frameWidth * 4 / 15;
		this.h = Main.frameWidth / 15;
		this.r = 10;
		this.borderColor = Color.BLACK;
		this.normalColor = new Color(103, 130, 58);
		this.pressedColor = new Color(85, 58, 130);
		this.overColor = Color.WHITE;
		this.text = text;
		this.textNormalColor = Color.WHITE;
		this.textPressedColor = Color.WHITE;
		this.textOverColor = new Color(103, 130, 58);
		this.textSize = h / 3;
		this.fontName = "Dialog";
		this.setPreferredSize(new Dimension(w, h));
		this.setBounds(0, 0, w, h);
	}

	protected void processMouseEvent(MouseEvent e) {
		super.processMouseEvent(e);
		if (e.getID() == MouseEvent.MOUSE_PRESSED) {
			mousePressed = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_RELEASED) {
			mousePressed = false;
			if (mouseOver) {
				this.main.click();
			}
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_ENTERED) {
			mouseOver = true;
			this.repaint();
		}
		if (e.getID() == MouseEvent.MOUSE_EXITED) {
			mouseOver = false;
			this.repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		paintComponent(g);
		paintBorder(g);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		if (mousePressed) {
			g2D.setColor(this.pressedColor);
		} else {
			if (mouseOver) {
				g2D.setColor(this.overColor);
			} else {
				g2D.setColor(this.normalColor);
			}
		}
		g2D.fillRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
		drawStringCentered(g);
	}

	public void drawStringCentered(Graphics g) {
		g.setFont(new Font(this.fontName, Font.BOLD, textSize));
		Rectangle2D r2D = g.getFont().getStringBounds(this.text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = (this.w / 2) - (rW / 2) - rX;
		int b = (this.h / 2) - (rH / 2) - rY;
		if (mousePressed) {
			g.setColor(this.textPressedColor);
		} else {
			if (mouseOver) {
				g.setColor(this.textOverColor);
			} else {
				g.setColor(this.textNormalColor);
			}
		}
		g.drawString(this.text, a, b);
	}

	@Override
	protected void paintBorder(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(this.borderColor);
		g2D.drawRoundRect(0, 0, this.w - 2, this.h - 2, this.r, this.r);
	}
}