package ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import facts.Facts.Fact;
import objects.Agent;
import objects.Elements.Element;
import objects.Environment;

public class PanelForest extends JPanel {
	private static final long serialVersionUID = 1L;

	private Environment env;
	private Agent agent;

	public PanelForest(Environment env, Agent agent) {
		this.env = env;
		this.agent = agent;
		this.setBackground(Color.BLACK);
		this.setPreferredSize(new Dimension(Main.frameWidth * 17 / 20, Main.frameHeight * 17 / 20));
	}

	public void newEnv() {
		this.env.newEnv();
		this.repaint();
	}

	@Override
	public void paint(Graphics g) {
		int size = this.env.getForest().getSize();

		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(2));
		g2.draw(new Line2D.Float(1, 0, 1, this.getHeight() - 1));
		g2.draw(new Line2D.Float(0, 0, this.getWidth() - 1, 0));
		g2.draw(new Line2D.Float(this.getWidth() - 1, 0, this.getWidth() - 1, this.getHeight() - 1));
		g2.draw(new Line2D.Float(0, this.getHeight() - 1, this.getWidth() - 1, this.getHeight() - 1));

		g2.setStroke(new BasicStroke(1));
		for (int i = 1; i < size; i++) {
			g2.draw(new Line2D.Float(i * this.getWidth() / size, 0, i * this.getWidth() / size, this.getHeight()));
			g2.draw(new Line2D.Float(0, i * this.getHeight() / size, this.getWidth(), i * this.getHeight() / size));
		}

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (this.env.getForest().getGrid()[i][j].getList().size() > 1) {
					if (this.env.getForest().getGrid()[i][j].getList().contains(Element.PLAYER)) {
						for (Element e : this.env.getForest().getGrid()[i][j].getList()) {
							drawImage(g, e, this.getWidth() / (size * 2) + i * this.getWidth() / size,
									this.getHeight() / (size * 2) + j * this.getHeight() / size);
						}
					} else {
						for (Element e : this.env.getForest().getGrid()[i][j].getList()) {
							drawBigImage(g, e, this.getWidth() / (size * 2) + i * this.getWidth() / size,
									this.getHeight() / (size * 2) + j * this.getHeight() / size);
						}
					}
				} else {
					for (Element e : this.env.getForest().getGrid()[i][j].getList()) {
						drawSingleImage(g, e, this.getWidth() / (size * 2) + i * this.getWidth() / size,
								this.getHeight() / (size * 2) + j * this.getHeight() / size);
					}
				}
			}
		}

		g.setColor(new Color(128, 128, 128, 128));
		for (int i = 0; i < agent.getFactsList().size(); i++) {
			if (agent.getFactsList().get(i).getFact().equals(Fact.NOTVISITED)) {
				g.fillRect(1 + agent.getFactsList().get(i).getX() * this.getWidth() / size,
						1 + agent.getFactsList().get(i).getY() * this.getHeight() / size, this.getWidth() / size,
						this.getHeight() / size);
			}
		}
	}

	public void drawStringCentered(Graphics g, int x, int y, String text) {
		g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 300 / this.env.getForest().getSize()));
		Rectangle2D r2D = g.getFont().getStringBounds(text, g.getFontMetrics().getFontRenderContext());
		int rW = (int) Math.round(r2D.getWidth());
		int rH = (int) Math.round(r2D.getHeight());
		int rX = (int) Math.round(r2D.getX());
		int rY = (int) Math.round(r2D.getY());
		int a = x - (rW / 2) - rX;
		int b = y - (rH / 2) - rY;
		g.drawString(text, a, b);
	}

	public void drawSingleImage(Graphics g, Element e, int x, int y) {
		int size = this.env.getForest().getSize();
		switch (e) {
		case SMELL:
			drawImageCentered(g, new ImageIcon("src/images/smell.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		case WIND:
			drawImageCentered(g, new ImageIcon("src/images/wind.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;

		case MONSTER:
			drawImageCentered(g, new ImageIcon("src/images/monster.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		case HOLE:
			drawImageCentered(g, new ImageIcon("src/images/hole.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;

		case LIGHT:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		case PORTAL:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;

		case PLAYER:
			drawImageCentered(g, new ImageIcon("src/images/player.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		default:
			break;
		}
	}

	public void drawImage(Graphics g, Element e, int x, int y) {
		int size = this.env.getForest().getSize();
		switch (e) {
		case SMELL:
			drawImageCentered(g, new ImageIcon("src/images/smell.png").getImage(), x - this.getWidth() / (size * 2),
					y - this.getHeight() / (size * 2), this.getWidth() / (size * 3), this.getHeight() / (size * 3));
			break;
		case WIND:
			drawImageCentered(g, new ImageIcon("src/images/wind.png").getImage(), x + this.getWidth() / (size * 6),
					y - this.getHeight() / (size * 2), this.getWidth() / (size * 3), this.getHeight() / (size * 3));
			break;

		case MONSTER:
			drawImageCentered(g, new ImageIcon("src/images/monster.png").getImage(), x - this.getWidth() / (size * 2),
					y + this.getHeight() / (size * 6), this.getWidth() / (size * 3), this.getHeight() / (size * 3));
			break;
		case HOLE:
			drawImageCentered(g, new ImageIcon("src/images/hole.png").getImage(), x + this.getWidth() / (size * 6),
					y + this.getHeight() / (size * 6), this.getWidth() / (size * 3), this.getHeight() / (size * 3));
			break;

		case LIGHT:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		case PORTAL:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;

		case PLAYER:
			drawImageCentered(g, new ImageIcon("src/images/player.png").getImage(), x - this.getWidth() / (size * 4),
					y - this.getHeight() / (size * 4), this.getWidth() / (size * 2), this.getHeight() / (size * 2));
			break;
		default:
			break;
		}
	}

	public void drawBigImage(Graphics g, Element e, int x, int y) {
		int size = this.env.getForest().getSize();
		switch (e) {
		case SMELL:
			drawImageCentered(g, new ImageIcon("src/images/smell.png").getImage(), x - this.getWidth() / (size * 2),
					y - this.getHeight() / (size * 2), this.getWidth() / (size * 2), this.getHeight() / (size * 2));
			break;
		case WIND:
			drawImageCentered(g, new ImageIcon("src/images/wind.png").getImage(), x, y - this.getHeight() / (size * 2),
					this.getWidth() / (size * 2), this.getHeight() / (size * 2));
			break;

		case MONSTER:
			drawImageCentered(g, new ImageIcon("src/images/monster.png").getImage(), x - this.getWidth() / (size * 2),
					y, this.getWidth() / (size * 2), this.getHeight() / (size * 2));
			break;
		case HOLE:
			drawImageCentered(g, new ImageIcon("src/images/hole.png").getImage(), x, y, this.getWidth() / (size * 2),
					this.getHeight() / (size * 2));
			break;

		case LIGHT:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;
		case PORTAL:
			drawImageCentered(g, new ImageIcon("src/images/portal.png").getImage(), x - this.getWidth() / (size * 3),
					y - this.getHeight() / (size * 3), (int) (this.getWidth() / (size * 1.5)),
					(int) (this.getHeight() / (size * 1.5)));
			break;

		case PLAYER:
			break;
		default:
			break;
		}
	}

	public void drawImageCentered(Graphics g, Image i, int x, int y, int w, int h) {
		g.drawImage(i, x, y, w, h, null);
	}
}