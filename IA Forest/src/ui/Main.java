package ui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import objects.Agent;
import objects.Environment;
import rules.InferencesEngine;

public class Main {
	protected static final int frameWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 80;
	protected static final int frameHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - 80;

	private JFrame frame;
	private JPanel panel;
	private PanelForest panelForet;
	private Button button;
	private JLabel label;

	private Environment env;
	private Agent agent;
	private InferencesEngine engine;

	public void click() {
		this.engine.process();
		this.label.setText("Performances : " + this.agent.getPerformance());
		this.frame.repaint();
	}

	public void doGraphics() {
		this.frame = new JFrame("Travail #3 : Agent logique - Foret magique");
		this.panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setSize(Main.frameWidth, Main.frameHeight);

		((FlowLayout) panel.getLayout()).setHgap(105);
		((FlowLayout) panel.getLayout()).setVgap(10);

		this.env = new Environment();
		this.agent = new Agent(this.env);
		this.engine = new InferencesEngine(this.agent);

		this.panelForet = new PanelForest(this.env, this.agent);
		panel.add(this.panelForet);

		this.button = new Button("Bouger", this);
		this.button.setLocation(Main.frameWidth / 10, Main.frameHeight * 9 / 10);
		panel.add(this.button);

		this.label = new JLabel("Performances : " + this.agent.getPerformance());
		this.label.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
		panel.add(this.label);

		frame.add(panel);
		frame.getContentPane();
		frame.setSize(Main.frameWidth, Main.frameHeight);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		Main main = new Main();
		main.doGraphics();
	}
}